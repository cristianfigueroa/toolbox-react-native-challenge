
# Toolbox Challenge
------------

## Antes de comenzar se tiene que instalar estas herramientas.
-------
* [Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git)
* Consola [react native](https://reactnative.dev/docs/environment-setup "react native"), seguir los pasos hasta la sección ***Creating a new application***
* [Yarn](https://yarnpkg.com/getting-started/install).
* [Android Studio](https://developer.android.com/studio).

### Para compilar IOS (solo en Mac).
* [Xcode](https://apps.apple.com/ar/app/xcode/id497799835?mt=12)
* [CocoaPods](https://cocoapods.org/)

------------
## Creación de emulador de android.
-------
**Importante: Se tiene que tener instalado Android Studio**
  * Seguir los pasos que se describen aca [crear emulador](https://developer.android.com/studio/run/managing-avds?hl=es-419)

------
## Compilación e instalacion en emulador.

* Bajar el proyecto desde bitbucket con este comando:
`git clone https://cristianfigueroa@bitbucket.org/cristianfigueroa/toolbox-react-native-challenge.git`

* Ingresar a la carpeta Toolbox `cd Toolbox` y bajar las dependencias del proyecto con este comando: `yarn install`.
* Si se quiere compilar el proyecto para iOS se tiene que correr estos comandos: `cd ios $$ pod install`
  
### Iniciar la app
* Android `yarn android`.
* iOS `yarn ios`.

***Nota: Para ingresar a la app, en el campo correo electronico se tiene que ingresar un correo electronico válido.***


---------
## Bibliotecas usadas
### Navegacion
* [react navigation](https://reactnavigation.org/docs/getting-started)
* [react-native-screens](https://github.com/software-mansion/react-native-screens)
* [react-native-safe-area-context](https://github.com/th3rdwave/react-native-safe-area-context#readme)

### Imagenes
* [Fast Image](https://github.com/DylanVann/react-native-fast-image)

### Almacenamiento
* [Async Storage](https://github.com/react-native-async-storage/async-storage)

### Conectividad
* [Axios](https://axios-http.com/docs/intro)

### Video 
* [react native video](https://github.com/react-native-video/react-native-video)

### Contexto
* [redux](https://es.redux.js.org/)
* [redux-toolkit](https://redux-toolkit.js.org/introduction/getting-started)

-------

