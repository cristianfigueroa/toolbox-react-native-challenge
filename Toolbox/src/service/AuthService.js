import AsyncStorage from '@react-native-async-storage/async-storage';
import {post} from '../config/Api';
import Strings from '../styles/strings';
import MessageError from '../utils/messageError';

const TOKEN_KEY = '@AUTH_TOKEN';

export const login = async () => {
  try {
    const response = await post('v1/mobile/auth', {sub: 'ToolboxMobileTest'});
    saveToken(response.data);
    return response.data;
  } catch (error) {
    console.log('Error calling login service', error);
    MessageError.data = error;
    MessageError.message = Strings.defaultError;
    throw MessageError;
  }
};

const saveToken = async token => {
  return AsyncStorage.setItem(TOKEN_KEY, JSON.stringify(token));
};

export const removeToken = async () => {
  const token = await getToken();
  if (token) {
    return AsyncStorage.removeItem(TOKEN_KEY);
  }
};

export const getToken = async () => {
  const result = await AsyncStorage.getItem(TOKEN_KEY);
  return result ? JSON.parse(result) : {};
};
