import {get} from '../config/Api';
import Strings from '../styles/strings';
import MessageError from '../utils/messageError';

export const getVideos = async () => {
  try {
    const response = await get('v1/mobile/data');
    let result = response.data.map(item => {
      return {
        title: item.title,
        type: item.type,
        data: [{items: item.items}],
      };
    });
    return result;
  } catch (error) {
    console.log('Error at retrieve list of videos, message error', error);
    MessageError.data = error;
    MessageError.message = Strings.defaultError;
    throw MessageError;
  }
};
