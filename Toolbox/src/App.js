/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import Colors from './styles/colors';
import AppStack from './screens/navigators/app.navigators';
import {Provider} from 'react-redux';
import {store} from './utils/store';

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer theme={{colors: {background: Colors.white}}}>
        <AppStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
