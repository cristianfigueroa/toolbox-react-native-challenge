import {Alert} from 'react-native';

export const validateEmail = email => {
  const regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return regexEmail.test(email);
};

export const showErrorAlert = text => {
  Alert.alert('', text);
};
export const isEmptyString = value => {
  return value === null || value === '' || value === undefined;
};
