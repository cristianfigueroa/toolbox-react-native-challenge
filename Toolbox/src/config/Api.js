import axios from 'axios';
import {getToken, login, removeToken} from '../service/AuthService';

const axiosInstance = axios.create({
  baseURL: 'https://echo-serv.tbxnet.com/',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const get = async url => {
  return axiosInstance.get(url);
};

export const post = async (url, request) => {
  return axiosInstance.post(url, {...request});
};
axiosInstance.interceptors.request.use(
  async config => {
    const {type, token} = await getToken();
    if (token && !config.url.includes('auth')) {
      config.headers.Authorization = `${type} ${token}`;
    }
    return config;
  },
  error => {
    console.log('Error retrieve token', error);
    Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    const originalRequest = error.config;
    if (error.response.status === 401) {
      await refreshToken();
      return axiosInstance(originalRequest);
    }
    return Promise.reject(error);
  },
);
const refreshToken = async () => {
  await removeToken();
  await login();
};
