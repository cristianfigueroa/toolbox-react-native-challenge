import {StackActions} from '@react-navigation/native';
import React, {useState} from 'react';
import {ActivityIndicator, TextInput, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {login} from '../../service/AuthService';
import {getVideos} from '../../service/VideoService';
import Strings from '../../styles/strings';
import {setVideos} from '../../utils/store/videoReducer';
import {showErrorAlert, validateEmail} from '../../utils/Utils';
import CustomButton from '../component/customButton';
import Routes from '../navigators/Routes';
import styles from './styles';

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    if (validateEmail(email.trim())) {
      setLoading(true);
      try {
        await login();
        const listVideos = await getVideos();
        dispatch(setVideos(listVideos));
        setLoading(false);
        navigation.dispatch(StackActions.replace(Routes.homeStack));
      } catch (error) {
        showErrorAlert(error.message);
      }
    } else {
      showErrorAlert(Strings.emailError);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <TextInput
          onChangeText={setEmail}
          style={styles.input}
          placeholder={Strings.emailPlaceholder}
        />
      </View>
      <View style={styles.footer}>
        {loading ? (
          <ActivityIndicator size={'large'} />
        ) : (
          <CustomButton onPress={() => handleSubmit()} />
        )}
      </View>
    </View>
  );
};
export default LoginScreen;
