import {StyleSheet} from 'react-native';
import Colors from '../../styles/colors';

const loginStyle = StyleSheet.create({
  button: {
    height: 44,
    width: '100%',
    backgroundColor: Colors.purple,
    alignContent: 'center',
    justifyContent: 'center',
    borderRadius: 6,
  },
  labelButton: {
    fontSize: 18,
    color: 'white',
    fontWeight: '500',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  container: {flex: 1, marginHorizontal: 28, backgroundColor: Colors.white},
  body: {flex: 0.8, justifyContent: 'center'},
  footer: {flex: 0.2, alignItems: 'center', justifyContent: 'center'},
  input: {
    height: 40,
    fontSize: 18,
    margin: 12,
    borderBottomWidth: 0.8,
    padding: 10,
  },
});

export default loginStyle;
