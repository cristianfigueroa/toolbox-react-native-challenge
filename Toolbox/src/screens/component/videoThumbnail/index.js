import React from 'react';
import {View} from 'react-native';
import FastImage from 'react-native-fast-image';

const VideoThumbnail = ({cacheKey, url, width, height}) => {
  return (
    <View style={{width, height}}>
      <FastImage
        style={{width, height}}
        source={{
          uri: `${url}_${cacheKey}`,
          priority: FastImage.priority.high,
        }}
      />
    </View>
  );
};

export default VideoThumbnail;
