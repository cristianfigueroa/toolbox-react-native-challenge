import React from 'react';
import {View, Platform, TouchableWithoutFeedback} from 'react-native';
import cardStyle from './styles';

const Card = ({children, style, onPress}) => {
  const styles =
    Platform.OS === 'android'
      ? cardStyle.containerAndroid
      : cardStyle.containerIos;

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles, style]}>{children}</View>
    </TouchableWithoutFeedback>
  );
};

export default Card;
