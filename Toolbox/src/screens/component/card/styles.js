import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const BACKGROUND_COLOR = Colors.white;
const ELEVATION = 5;
const OPACITY = 0.5;

const cardStyle = StyleSheet.create({
  containerIos: {
    shadowRadius: ELEVATION,
    shadowOpacity: OPACITY,
    shadowOffset: {width: 0, height: ELEVATION},
    backgroundColor: BACKGROUND_COLOR,
  },
  containerAndroid: {
    elevation: ELEVATION,
    backgroundColor: BACKGROUND_COLOR,
  },
});
export default cardStyle;
