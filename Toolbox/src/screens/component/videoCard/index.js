import React from 'react';
import {Text, View} from 'react-native';
import Card from '../card';
import VideoThumbnail from '../videoThumbnail';
import {useNavigation} from '@react-navigation/native';
import styles, {IMAGE_HEIGHT, IMAGE_WIDTH} from './styles';
import Routes from '../../navigators/Routes';
import {isEmptyString, showErrorAlert} from '../../../utils/Utils';
import Strings from '../../../styles/strings';

const config = {
  thumb: {
    width: IMAGE_WIDTH,
    height: IMAGE_HEIGHT + 40,
  },
  poster: {
    width: IMAGE_HEIGHT,
    height: IMAGE_WIDTH,
  },
};

const VideoCard = ({video, type}) => {
  const navigation = useNavigation();
  const {title, imageUrl, videoUrl} = video;
  const {width, height} = config[type];
  const cacheKey = `${title}_${type}`;

  const VideoThumb = () => (
    <View style={styles.thumbContainer}>
      <VideoThumbnail
        cacheKey={cacheKey}
        width={width}
        height={height - 40}
        url={imageUrl}
      />
      <Text style={styles.thumbLabel}>{title}</Text>
    </View>
  );

  const VideoPoster = () => (
    <>
      <VideoThumbnail
        cacheKey={cacheKey}
        width={width}
        height={height}
        url={imageUrl}
      />
      <Text style={styles.posterLabel}>{title}</Text>
    </>
  );
  const handlerOnPress = () => {
    if (isEmptyString(videoUrl)) {
      showErrorAlert(Strings.videoUrlEmpty);
    } else {
      navigation.navigate(Routes.video, {video});
    }
  };

  return (
    <Card
      style={{width, height, marginHorizontal: 16}}
      onPress={handlerOnPress}>
      {type === 'thumb' ? <VideoThumb /> : <VideoPoster />}
    </Card>
  );
};

export default VideoCard;
