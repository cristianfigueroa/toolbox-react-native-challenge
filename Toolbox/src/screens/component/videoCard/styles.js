import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

export const IMAGE_WIDTH = 260;
export const IMAGE_HEIGHT = 160;

const videoCardStyle = StyleSheet.create({
  posterLabel: {
    position: 'absolute',
    top: 230,
    alignSelf: 'center',
    color: Colors.black,
    fontWeight: 'bold',
    fontSize: 18,
  },
  thumbLabel: {
    color: Colors.black,
    marginTop: 8,
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  thumbContainer: {alignContent: 'center', alignItems: 'center'},
});

export default videoCardStyle;
