import React from 'react';
import {Pressable, Text} from 'react-native';
import Strings from '../../../styles/strings';

import customButtonStyle from './styles';

const CustomButton = ({onPress, style}) => {
  return (
    <Pressable style={[customButtonStyle.button, style]} onPress={onPress}>
      <Text style={customButtonStyle.labelButton}>{Strings.submit}</Text>
    </Pressable>
  );
};

export default CustomButton;
