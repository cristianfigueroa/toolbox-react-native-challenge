import {StyleSheet} from 'react-native';
import Colors from '../../../styles/colors';

const customButtonStyle = StyleSheet.create({
  button: {
    height: 44,
    width: '100%',
    backgroundColor: Colors.purple,
    alignContent: 'center',
    justifyContent: 'center',
    borderRadius: 6,
  },
  labelButton: {
    fontSize: 18,
    color: 'white',
    fontWeight: '500',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
});

export default customButtonStyle;
