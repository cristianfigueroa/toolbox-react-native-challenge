import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Colors from '../../styles/colors';
import Strings from '../../styles/strings';
import Routes from './Routes';
import LoginStack from './login.navigators';
import HomeStack from './home.navigators';

const Stack = createNativeStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.purple,
        },
        title: Strings.titleApp,
        headerTintColor: Colors.white,
      }}>
      <Stack.Screen name={Routes.loginStack} component={LoginStack} />
      <Stack.Screen
        name={Routes.homeStack}
        component={HomeStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default AppStack;
