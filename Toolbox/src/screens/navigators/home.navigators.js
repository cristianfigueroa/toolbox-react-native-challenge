import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Colors from '../../styles/colors';
import Strings from '../../styles/strings';
import HomeScreen from '../home';
import Routes from './Routes';
import VideoScreen from '../video';
import {Platform} from 'react-native';

const Stack = createNativeStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        title: Strings.titleApp,
        headerTintColor: Colors.white,
        headerBackTitle: '',
      }}>
      <Stack.Screen
        name={Routes.home}
        component={HomeScreen}
        options={{
          orientation: 'portrait',
          headerStyle: {
            backgroundColor: Colors.purple,
          },
        }}
      />
      <Stack.Screen
        name={Routes.video}
        component={VideoScreen}
        options={{
          orientation: Platform.OS === 'android' ? 'landscape' : 'portrait',
          headerStyle: {
            backgroundColor: Colors.black,
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
