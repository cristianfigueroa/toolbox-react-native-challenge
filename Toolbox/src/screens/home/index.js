import React from 'react';
import {FlatList, SectionList, Text, View, RefreshControl} from 'react-native';
import {useSelector} from 'react-redux';
import {getVideos} from '../../service/VideoService';
import VideoCard from '../component/videoCard';
import styles from './styles';

const HomeScreen = () => {
  let {videos} = useSelector(state => state.videos);
  const [refreshing, setRefreshing] = React.useState(false);

  const renderListVideo = ({item: itemList, section}) => {
    return (
      <FlatList
        data={itemList.items}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.list}
        renderItem={({item}) => renderVideo({item, section})}
        keyExtractor={keyExtractor}
      />
    );
  };

  const renderSectionHeader = ({section}) => {
    return <Text style={styles.header}>{section.title}</Text>;
  };

  const renderVideo = ({item, section}) => {
    return <VideoCard video={item} type={section.type} />;
  };

  const keyExtractor = item => {
    return item.title;
  };
  const loadVideos = async () => {
    videos = await getVideos();
    setRefreshing(false);
  };

  return (
    <View style={styles.container}>
      <SectionList
        sections={videos}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => loadVideos()}
          />
        }
        renderSectionHeader={renderSectionHeader}
        renderItem={renderListVideo}
      />
    </View>
  );
};
export default HomeScreen;
