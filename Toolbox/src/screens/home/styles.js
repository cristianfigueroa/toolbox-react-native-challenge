import {StyleSheet} from 'react-native';
import Colors from '../../styles/colors';

const homeStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.light_grey,
    paddingHorizontal: 8,
  },
  body: {flex: 0.8, justifyContent: 'center'},
  footer: {flex: 0.2, alignItems: 'center', justifyContent: 'center'},
  header: {
    fontSize: 24,
    color: Colors.black,
    fontWeight: 'bold',
    marginVertical: 16,
  },
  list: {marginBottom: 18},
});

export default homeStyle;
