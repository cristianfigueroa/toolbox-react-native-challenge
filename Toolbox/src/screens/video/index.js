import React from 'react';
import {Platform, View} from 'react-native';
import Video from 'react-native-video';

import videoViewStyle from './styles';

const VideoScreen = ({route}) => {
  const {
    video: {videoUrl},
  } = route.params;

  return (
    <View style={videoViewStyle.container}>
      <Video
        allowsExternalPlayback={false}
        source={{uri: videoUrl}}
        style={videoViewStyle.video}
        resizeMode={Platform.OS === 'android' ? 'cover' : 'contain'}
        controls={true}
      />
    </View>
  );
};

export default VideoScreen;
