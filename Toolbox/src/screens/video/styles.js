import {StyleSheet} from 'react-native';
import Colors from '../../styles/colors';

const videoViewStyle = StyleSheet.create({
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
});
export default videoViewStyle;
