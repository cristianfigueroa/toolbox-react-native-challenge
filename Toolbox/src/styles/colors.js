const Colors = {
  purple: '#590DE5',
  blue: '#3204AC',
  white: 'white',
  light_grey: '#E5E5E6',
  black: 'black',
  transparent: '#0000',
};

export default Colors;
