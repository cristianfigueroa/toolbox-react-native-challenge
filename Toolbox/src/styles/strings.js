const Strings = {
  titleApp: 'ToolBox',
  emailPlaceholder: 'Correo electrónico',
  submit: 'Ingresar',
  emailError: 'El correo electrónico es invalido',
  videoUrlEmpty: 'No se pudo reproducir el video',
  defaultError:'En estos momentos no podemos ingresar a su información.\nIntente nuevamente en unos minutos.',
};

export default Strings;
